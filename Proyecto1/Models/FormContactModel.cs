﻿using System;

namespace Proyecto1.Models
{
    public class FormContactModel
    {
        public String FirstName { get; set; }
        public String SecondName { get; set; }
        public String Email { get; set; }
        public String Message { get; set; }
    }
}