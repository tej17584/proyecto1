﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Proyecto1.Models;

namespace Proyecto1.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult IndexCV()
        {
            return View();
        }

        [HttpGet]
        public IActionResult FormCV()
        {
            return View();
        }
        [HttpPost]
        public IActionResult FormCV(FormContactModel model)
        {
            return Content($"Hello {model.FirstName + " " + model.SecondName + " " + model.Message + " " + model.Email}");
        }

    }
}
